# API Transportador (Floricultura)

## Estrutura

<img src="spring-micro-servicos-alura.png" alt="Estrutura dos Microservices">

 - Teremos três microsserviços: Fornecedor, Loja e Transportador

 - Uma apresentação da modelagem focado em DDD (Domain Driven Design)
     - Quebraremos o domínio em contextos menores (bounded context)
     - Um microsserviço é a implementação de um contexto

